#!/usr/bin/env python

import logbook
import os
import sys
from mako.lookup import TemplateLookup
from logbook import Logger, StreamHandler, RotatingFileHandler
logbook.set_datetime_format('local')

from conf import DEBUG, MV, LOG_PATH

try:
    from conf import DEFAULT_PASSWORD
except ImportError:
    DEFAULT_PASSWORD = 'salgueiro3.3'

try:
    from conf import SEAFILE_SERVER
except ImportError:
    SEAFILE_SERVER = {}

try:
    from conf import TITLE_APP
except ImportError:
    TITLE_APP = 'Empresa Libre'

try:
    from conf import NO_HTTPS
except ImportError:
    NO_HTTPS = True


DEBUG = DEBUG
VERSION = '1.5.0'
EMAIL_SUPPORT = ('soporte@empresalibre.net',)

BASE_DIR = os.path.abspath(os.path.dirname(__file__))

PATH_STATIC = os.path.abspath(os.path.join(BASE_DIR, '..'))
PATH_TEMPLATES = os.path.abspath(os.path.join(BASE_DIR, '..', 'templates'))
PATH_MEDIA = os.path.abspath(os.path.join(BASE_DIR, '..', 'docs'))

PATH_CP = os.path.abspath(os.path.join(BASE_DIR, '..', 'db', 'cp.db'))
COMPANIES = os.path.abspath(os.path.join(BASE_DIR, '..', 'db', 'rfc.db'))
DB_SAT = os.path.abspath(os.path.join(BASE_DIR, '..', 'db', 'sat.db'))

PATH_SESSIONS = {
    'data': os.path.abspath(os.path.join(BASE_DIR, '..', 'cache', 'data')),
    'lock': os.path.abspath(os.path.join(BASE_DIR, '..', 'cache', 'lock')),
}

IV = 'valores_iniciales.json'
INIT_VALUES = os.path.abspath(os.path.join(BASE_DIR, '..', 'db', IV))
CT = 'cancel_template.xml'
TEMPLATE_CANCEL = os.path.abspath(os.path.join(PATH_TEMPLATES, CT))

PATH_XSLT = os.path.abspath(os.path.join(BASE_DIR, '..', 'xslt'))
PATH_BIN = os.path.abspath(os.path.join(BASE_DIR, '..', 'bin'))

template_lookup = TemplateLookup(directories=[PATH_TEMPLATES],
    input_encoding='utf-8',
    output_encoding='utf-8')

LOG_PATH = 'empresalibre.log'
LOG_NAME = 'API'
LOG_LEVEL = 'INFO'

format_string = '[{record.time:%d-%b-%Y %H:%M:%S}] ' \
    '{record.level_name}: ' \
    '{record.channel}: ' \
    '{record.message}'


if DEBUG:
    LOG_LEVEL = 'DEBUG'
    StreamHandler(
        sys.stdout,
        level=LOG_LEVEL,
        format_string=format_string).push_application()
else:
    RotatingFileHandler(
        LOG_PATH,
        backup_count=10,
        max_size=1073741824,
        level=LOG_LEVEL,
        format_string=format_string).push_application()

    StreamHandler(
        sys.stdout,
        level=LOG_LEVEL,
        format_string=format_string).push_application()


log = Logger(LOG_NAME)


PATH_XSLTPROC = 'xsltproc'
PATH_OPENSSL = 'openssl'
PATH_XMLSEC = 'xmlsec1'
if 'win' in sys.platform:
    PATH_XSLTPROC = os.path.join(PATH_BIN, 'xsltproc.exe')
    PATH_OPENSSL = os.path.join(PATH_BIN, 'openssl.exe')
    PATH_XMLSEC = os.path.join(PATH_BIN, 'xmlsec.exe')


PRE = {
    '2.0': '{http://www.sat.gob.mx/cfd/2}',
    '2.2': '{http://www.sat.gob.mx/cfd/2}',
    '3.0': '{http://www.sat.gob.mx/cfd/3}',
    '3.2': '{http://www.sat.gob.mx/cfd/3}',
    '3.3': '{http://www.sat.gob.mx/cfd/3}',
    'TIMBRE': '{http://www.sat.gob.mx/TimbreFiscalDigital}',
    'DONATARIA': '{http://www.sat.gob.mx/donat}',
    'INE': '{http://www.sat.gob.mx/ine}',
    'LOCALES': '{http://www.sat.gob.mx/implocal}',
    'NOMINA': {
        '1.1': '{http://www.sat.gob.mx/nomina}',
        '1.2': '{http://www.sat.gob.mx/nomina12}',
        }
}

CURRENT_CFDI = '3.3'
CURRENT_CFDI_NOMINA = '1.2'
DECIMALES = 2
DECIMALES_TAX = 4
IMPUESTOS = {
    'ISR': '001',
    'IVA': '002',
    'IEPS': '003',
    'EXENTO': '000',
    'ISH': '000',
    'INSPECCION DE OBRA': '000',
    'ICIC': '000',
    'CEDULAR': '000',
    'CMIC': '000',
    'SUPERVISION': '000',
}
DEFAULT_SAT_PRODUCTO = '01010101'
DEFAULT_SERIE_TICKET = 'T'
DIR_FACTURAS = 'facturas'
USAR_TOKEN = False
CANCEL_SIGNATURE = False
PUBLIC = 'Público en general'
DEFAULT_SAT_NOMINA = {
    'SERIE': 'N',
    'FORMA_PAGO': '99',
    'USO_CFDI': 'P01',
    'CLAVE': '84111505',
    'UNIDAD': 'ACT',
    'DESCRIPCION': 'Pago de nómina',
}

API = 'http://139.162.255.71:4043{}'
if DEBUG:
    API = 'http://127.0.0.1:8080{}'