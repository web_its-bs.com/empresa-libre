var query = []
var cfg_nomina = new Object()


var nomina_controllers = {
    init: function(){
        $$('cmd_nomina_import').attachEvent('onItemClick', cmd_nomina_import_click)
        $$('cmd_empleados').attachEvent('onItemClick', cmd_empleados_click)
        $$('cmd_close_empleados').attachEvent('onItemClick', cmd_close_empleados_click)
        $$('cmd_delete_empleado').attachEvent('onItemClick', cmd_delete_empleado_click)
        $$('cmd_import_empleados').attachEvent('onItemClick', cmd_import_empleados_click)
        $$('cmd_nomina_without_stamp').attachEvent('onItemClick', cmd_nomina_without_stamp_click)
        $$('cmd_nomina_delete').attachEvent('onItemClick', cmd_nomina_delete_click)
        $$('cmd_nomina_timbrar').attachEvent('onItemClick', cmd_nomina_timbrar_click)
        $$('cmd_nomina_log').attachEvent('onItemClick', cmd_nomina_log_click)
        $$('cmd_nomina_cancel').attachEvent('onItemClick', cmd_nomina_cancel_click)
        $$('grid_nomina').attachEvent('onItemClick', grid_nomina_click)
        $$('filter_year_nomina').attachEvent('onChange', filter_year_nomina_change)
        $$('filter_month_nomina').attachEvent('onChange', filter_month_nomina_change)
        $$('filter_dates_nomina').attachEvent('onChange', filter_dates_nomina_change)
        webix.extend($$('grid_nomina'), webix.ProgressBar)
    }
}


function default_config_nomina(){
    current_dates_nomina()
    get_nomina()
}


function current_dates_nomina(){
    var fy = $$('filter_year_nomina')
    var fm = $$('filter_month_nomina')
    var d = new Date()

    fy.blockEvent()
    fm.blockEvent()

    fm.setValue(d.getMonth() + 1)
    webix.ajax().sync().get('/values/filteryearsnomina', function(text, data){
        var values = data.json()
        fy.getList().parse(values)
        fy.setValue(d.getFullYear())
    })

    fy.unblockEvent()
    fm.unblockEvent()
}


function get_nomina(filters){
    var grid = $$('grid_nomina')
    grid.showProgress({type: 'icon'})


    webix.ajax().get('/nomina', filters, {
        error: function(text, data, xhr) {
            msg = 'Error al consultar'
            msg_error(msg)
        },
        success: function(text, data, xhr) {
            var values = data.json();
            if (values.ok){
                grid.clearAll();
                grid.parse(values.rows, 'json');
            }else{
                msg_error(values.msg)
            }
        }
    })
}


function cmd_nomina_import_click(){
    win_import_nomina.init()
    $$('win_import_nomina').show()
}


function cmd_import_template_nomina_click(){
    var form = $$('form_upload_nomina')

    var values = form.getValues()

    if(!$$('lst_upload_nomina').count()){
        $$('win_import_nomina').close()
        return
    }

    if($$('lst_upload_nomina').count() > 1){
        msg = 'Selecciona solo un archivo'
        msg_error(msg)
        return
    }

    var template = $$('up_nomina').files.getItem($$('up_nomina').files.getFirstId())

    if(template.type.toLowerCase() != 'ods'){
        msg = 'Archivo inválido.\n\nSe requiere un archivo ODS'
        msg_error(msg)
        return
    }

    msg = '¿Estás seguro de importar este archivo?'
    webix.confirm({
        title: 'Importar Nómina',
        ok: 'Si',
        cancel: 'No',
        type: 'confirm-error',
        text: msg,
        callback:function(result){
            if(result){
                $$('up_nomina').send()
            }
        }
    })
}


function up_nomina_upload_complete(response){
    if(response.status != 'server'){
        msg = 'Ocurrio un error al subir el archivo'
        msg_error(msg)
        return
    }
    msg = 'Archivo subido correctamente.\n\nComenzando importación.'
    msg_ok(msg)
    $$('win_import_nomina').close()

    webix.ajax().get('/nomina', {opt: 'import'}, {
        error: function(text, data, xhr) {
            msg = 'Error al importar'
            msg_error(msg)
        },
        success: function(text, data, xhr) {
            var values = data.json();
            if (values.ok){
                msg_ok(values.msg)
                get_nomina()
            }else{
                msg_error(values.msg)
            }
        }
    })
}


function get_employees(){
    webix.ajax().get('/employees', {
        error: function(text, data, xhr) {
            msg = 'Error al consultar'
            msg_error(msg)
        },
        success: function(text, data, xhr) {
            var values = data.json();
            if (values.ok){
                $$('grid_employees').clearAll();
                $$('grid_employees').parse(values.rows, 'json');
            }else{
                msg_error(values.msg)
            }
        }
    })
}


function cmd_empleados_click(){
    get_employees()
    $$('multi_nomina').setValue('nomina_empleados')
}


function cmd_close_empleados_click(){
    $$('multi_nomina').setValue('nomina_home')
}


function cmd_import_empleados_click(){
    win_import_employees.init()
    $$('win_import_employees').show()
}

function cmd_import_employees_click(){
    var form = $$('form_upload_employees')

    var values = form.getValues()

    if(!$$('lst_upload_employees').count()){
        $$('win_import_employees').close()
        return
    }

    if($$('lst_upload_employees').count() > 1){
        msg = 'Selecciona solo un archivo'
        msg_error(msg)
        return
    }

    var template = $$('up_employees').files.getItem($$('up_employees').files.getFirstId())

    if(template.type.toLowerCase() != 'ods'){
        msg = 'Archivo inválido.\n\nSe requiere un archivo ODS'
        msg_error(msg)
        return
    }

    msg = '¿Estás seguro de importar este archivo?'
    webix.confirm({
        title: 'Importar Empleados',
        ok: 'Si',
        cancel: 'No',
        type: 'confirm-error',
        text: msg,
        callback:function(result){
            if(result){
                $$('up_employees').send()
            }
        }
    })
}


function up_employees_upload_complete(response){
    if(response.status != 'server'){
        msg = 'Ocurrio un error al subir el archivo'
        msg_error(msg)
        return
    }
    msg = 'Archivo subido correctamente.\n\nComenzando importación.'
    msg_ok(msg)
    $$('win_import_employees').close()

    webix.ajax().get('/employees', {opt: 'import'}, {
        error: function(text, data, xhr) {
            msg = 'Error al importar'
            msg_error(msg)
        },
        success: function(text, data, xhr) {
            var values = data.json();
            if (values.ok){
                msg_ok(values.msg)
                get_employees()
            }else{
                msg_error(values.msg)
            }
        }
    })
}


function delete_empleado(id){
    webix.ajax().del('/employees', {id: id}, function(text, xml, xhr){
        var msg = 'Empleado eliminado correctamente'
        if (xhr.status == 200){
            $$('grid_employees').remove(id);
            msg_ok(msg)
        } else {
            msg = 'El Empleado tiene recibos timbrados'
            msg_error(msg)
        }
    })
}


function cmd_delete_empleado_click(){
    var row = $$('grid_employees').getSelectedItem()

    if (row == undefined){
        msg = 'Selecciona un Empleado'
        msg_error(msg)
        return
    }

    msg = '¿Estás seguro de eliminar al Empleado?<BR><BR>'
    msg += row['nombre_completo'] + ' (' + row['rfc'] + ')'
    msg += '<BR><BR>ESTA ACCIÓN NO SE PUEDE DESHACER<BR><BR>'
    webix.confirm({
        title: 'Eliminar Empleado',
        ok: 'Si',
        cancel: 'No',
        type: 'confirm-error',
        text: msg,
        callback:function(result){
            if (result){
                delete_empleado(row['id'])
            }
        }
    })
}


function cmd_nomina_without_stamp_click(){
    get_nomina()
}


function cmd_nomina_delete_click(){
    var rows = $$('grid_nomina').getSelectedItem()

    if (rows == undefined){
        msg = 'Selecciona al menos un registro'
        msg_error(msg)
        return
    }
    var ids = []
    if(Array.isArray(rows)){
        for(var i in rows){
            ids.push(rows[i].id)
        }
    }else{
        ids.push(rows.id)
    }

    msg = '¿Estás seguro de eliminar los recibos seleccionado?<BR><BR>'
    msg += 'ESTA ACCIÓN NO SE PUEDE DESHACER<BR><BR>'
    msg += 'Solo se eliminan recibos no timbrados'
    webix.confirm({
        title: 'Eliminar Nomina',
        ok: 'Si',
        cancel: 'No',
        type: 'confirm-error',
        text: msg,
        callback:function(result){
            if (result){
                delete_nomina(ids)
            }
        }
    })
}


function delete_nomina(ids){
    webix.ajax().del('/nomina', {id: ids}, function(text, xml, xhr){
        var msg = 'Registros eliminados correctamente'
        if (xhr.status == 200){
            get_nomina()
            msg_ok(msg)
        } else {
            msg = 'No se pudo eliminar.'
            msg_error(msg)
        }
    })
}


function cmd_nomina_timbrar_click(){
    get_nomina()

    msg = 'Se enviarán a timbrar todos los recibos sin timbrar<BR><BR>'
    msg += '¿Estás seguro de continuar?<BR><BR>'
    webix.confirm({
        title: 'Enviar a timbrar',
        ok: 'Si',
        cancel: 'No',
        type: 'confirm-error',
        text: msg,
        callback:function(result){
            if (result){
                timbrar_nomina()
            }
        }
    })
}


function timbrar_nomina(){
    webix.ajax().get('/nomina', {opt: 'stamp'}, {
        error: function(text, data, xhr) {
            msg = 'Error al timbrar'
            msg_error(msg)
        },
        success: function(text, data, xhr) {
            var values = data.json();
            if(values.ok){
                cmd_update_timbres_click()
                get_nomina()
                msg_ok(values.msg_ok)
            }
            if(values.error){
                webix.alert({
                    title: 'Error al Timbrar',
                    text: values.msg_error,
                    type: 'alert-error'
                })
            }
        }
    })
}


function grid_nomina_click(id, e, node){
    var row = this.getItem(id)

    if(id.column == 'xml'){
        location = '/doc/nomxml/' + row.id
    }else if(id.column == 'pdf'){
        window.open('/doc/nompdf/' + row.id, '_blank')
    //~ }else if(id.column == 'email'){
        //~ enviar_correo(row)
    }

}


function filter_year_nomina_change(nv, ov){
    var fm = $$('filter_month_nomina')
    filters = {'opt': 'yearmonth', 'year': nv, 'month': fm.getValue()}
    get_nomina(filters)
}


function filter_month_nomina_change(nv, ov){
    var fy = $$('filter_year_nomina')
    filters = {'opt': 'yearmonth', 'year': fy.getValue(), 'month': nv}
    get_nomina(filters)
}


function filter_dates_nomina_change(range){
    if(range.start != null && range.end != null){
        filters = {'opt': 'dates', 'range': range}
        get_nomina(filters)
    }
}


function cmd_nomina_cancel_click(){
    var row = $$('grid_nomina').getSelectedItem()

    if(row == undefined){
        msg = 'Selecciona un registro'
        msg_error(msg)
        return
    }

    if(Array.isArray(row)){
        msg = 'Selecciona solo un registro'
        msg_error(msg)
        return
    }

    if(row['estatus'] != 'Timbrado'){
        msg = 'Solo se pueden cancelar recibos timbrados'
        msg_error(msg)
        return
    }

    msg = '¿Estás seguro de cancelar el recibo?<BR><BR>'
    msg += row['empleado'] + ' (' + row['serie'] + '-' + row['folio'] + ')'
    msg += '<BR><BR>ESTA ACCIÓN NO SE PUEDE DESHACER<BR><BR>'
    webix.confirm({
        title: 'Cancelar Nomina',
        ok: 'Si',
        cancel: 'No',
        type: 'confirm-error',
        text: msg,
        callback:function(result){
            if (result){
                cancel_nomina(row['id'])
            }
        }
    })
}


function cancel_nomina(id){
    var grid = $$('grid_nomina')
    var data = new Object()
    data['opt'] = 'cancel'
    data['id'] = id

    webix.ajax().sync().post('nomina', data, {
        error:function(text, data, XmlHttpRequest){
            msg = 'Ocurrio un error, consulta a soporte técnico'
            msg_error(msg)
        },
        success:function(text, data, XmlHttpRequest){
            values = data.json();
            if(values.ok){
                grid.updateItem(id, values.row)
                msg_ok(values.msg)
            }else{
                msg_error(values.msg)
            }
        }
    })
}


function cmd_nomina_log_click(){
    location = '/doc/nomlog/0'
}