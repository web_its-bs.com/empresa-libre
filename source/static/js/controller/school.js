

var controllers_school = {
    init: function(){
        $$('cmd_new_student').attachEvent('onItemClick', cmd_new_student_click)
        $$('cmd_edit_student').attachEvent('onItemClick', cmd_edit_student_click)
        $$('cmd_delete_student').attachEvent('onItemClick', cmd_delete_student_click)
        $$('cmd_save_student').attachEvent('onItemClick', cmd_save_student_click)
        $$('cmd_cancel_student').attachEvent('onItemClick', cmd_cancel_student_click)
    }
}


function get_school_groups(){
    webix.ajax().get('/values/schoolgroups', {
        error: function(text, data, xhr) {
        },
        success: function(text, data, xhr) {
            var values = data.json();
            $$('student_grupo').define('suggest', values)
            $$('student_grupo').refresh()
        }
    })
}


function init_config_school(){
    get_students()
    get_school_groups()
}


function cmd_new_student_click(){
    $$('form_student').setValues({})
    $$('grid_students').clearSelection()
    $$('multi_school').setValue('new_student')
}


function cmd_edit_student_click(){
    var row = $$('grid_students').getSelectedItem()

    if (row == undefined){
        msg = 'Selecciona un Alumno'
        msg_error(msg)
        return
    }

    webix.ajax().get('/students', {id: row['id']}, {
        error: function(text, data, xhr) {
            msg_error()
        },
        success: function(text, data, xhr){
            var values = data.json()
            $$('form_student').setValues(values)
        }
    })

    $$('multi_school').setValue('new_student')
}


function delete_student(id){
    webix.ajax().del('/students', {id: id}, function(text, xml, xhr){
        msg = 'Alumno eliminado correctamente'
        if (xhr.status == 200){
            $$('grid_students').remove(id);
            msg_ok(msg)
        } else {
            msg = 'No se pudo eliminar.'
            msg_error(msg)
        }
    })
}


function cmd_delete_student_click(){
    var row = $$('grid_students').getSelectedItem()

    if (row == undefined){
        msg = 'Selecciona un Alumno'
        msg_error(msg)
        return
    }

    msg = '¿Estás seguro de eliminar al Alumno?<BR><BR>'
    msg += row['nombre'] + ' ' + row['paterno'] + ' (' + row['rfc'] + ')'
    msg += '<BR><BR>ESTA ACCIÓN NO SE PUEDE DESHACER<BR><BR>'
    webix.confirm({
        title:'Eliminar Alumno',
        ok:'Si',
        cancel:'No',
        type:'confirm-error',
        text:msg,
        callback:function(result){
            if (result){
                delete_student(row['id'])
            }
        }
    })
}


function cmd_cancel_student_click(){
    $$('multi_school').setValue('school_home')
}


function cmd_save_student_click(){
    msg = ''
    var form = this.getFormView();

    if (!form.validate()) {
        msg_error(msg)
        return
    }

    var values = form.getValues();
    opt = 'add'
    if(values.id){
        opt = 'edit'
    }

    webix.ajax().post('/students', {opt: opt, values: values}, {
        error:function(text, data, XmlHttpRequest){
            msg = 'Ocurrio un error, consulta a soporte técnico';
            msg_error(msg)
        },
        success:function(text, data, XmlHttpRequest){
            var values = data.json();
            if (values.ok) {
                form.setValues({})
                $$('multi_school').setValue('school_home')
                get_students()
            } else {
                msg_error(values.msg)
            }
        }
    })

}


function get_students(){
    webix.ajax().get('/students', {}, {
        error: function(text, data, xhr) {
            msg_error('Error al consultar')
        },
        success: function(text, data, xhr) {
            var values = data.json()
            $$('grid_students').clearAll()
            $$('grid_students').parse(values)
        }
    })
}